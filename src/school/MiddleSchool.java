package school;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class MiddleSchool {

    private final List<Grade> grades;
    private final Gson gson = new Gson();

    /**
     * Creates a new middleSchool with the associated grades
     */
    public MiddleSchool() {
        this.grades = new ArrayList<>();
        for (final Level level : Level.values()) {
            for (final Identifier identifier : Identifier.values()) {
                this.grades.add(new Grade(level.getLevel(), identifier.getIdentifier()));
            }
        }

    }

    /**
     * returns the list of the grades in the school
     * @return A list containing the grades {@link List}({@link Grade})
     */
    public List<Grade> getGrades() {
        return grades;
    }


    /**
     * Generates the Json of the school located in /res/
     */
    public void generateJson() {
        final String values = gson.toJson(grades);
        FileUtils.generateFile("res/middleschool.json", values);
    }

    /**
     * Returns the marks in all the subjects of an entire level
     *
     * @param gradeLevel {@link Integer} The {@link Grade} level
     * @return {@link List}({@link Grade}) A list of grades
     */
    public List<Grade> getGradesOfLevel(final int gradeLevel) {
        return grades.stream().filter(grade -> grade.getLevel() == gradeLevel).collect(Collectors.toList());
    }

    /**
     * Returns the minimum mark from a specified test
     * in a specified subject on a specified level
     *
     * @param subject    {@link Subject}
     * @param testIndex  {@link Integer}
     * @param gradeLevel {@link Integer}
     * @return The minimum {@link Float}
     */
    public Float getSpecificMinFromSubjectAndLevel(final Subject subject, final int testIndex, final int gradeLevel) {
        final List<Float> listOfMinGrades = new ArrayList<>();
        Float min = null;
        for (final Grade grade : getGradesOfLevel(gradeLevel)) {
            final Float minFromGrade = grade.getSpecificMinMarksFromSubject(subject, testIndex);
            if (minFromGrade != null) {
                listOfMinGrades.add(minFromGrade);
            }
        }
        if (!listOfMinGrades.isEmpty()) {
            min = Collections.min(listOfMinGrades);
        }
        return min;
    }


    /**
     * Returns the maximum mark from a specified test in a specified subject on a specified level
     *
     * @param subject    {@link Subject}
     * @param testIndex  {@link Integer}
     * @param gradeLevel {@link Integer}
     * @return {@link Float} the maximum
     */
    public Float getSpecificMaxFromSubjectAndLevel(final Subject subject, final int testIndex, final int gradeLevel) {
        final List<Float> listOfMaxGrades = new ArrayList<>();
        Float max = null;
        for (final Grade grade : getGradesOfLevel(gradeLevel)) {
            final Float maxFromGrade = grade.getSpecificMaxMarksFromSubject(subject, testIndex);
            if (maxFromGrade != null) {
                listOfMaxGrades.add(maxFromGrade);
            }
        }
        if (!listOfMaxGrades.isEmpty()) {
            max = Collections.max(listOfMaxGrades);
        }
        return max;
    }


    /**
     * Returns the average mark from a specified test
     * in a specified subject on a specified level
     *
     * @param subject    {@link Subject}
     * @param testIndex  {@link Integer}
     * @param gradeLevel {@link Integer}
     * @return {@link Float} the average mark
     */
    public Float getSpecificAvgFromSubjectAndLevel(final Subject subject, final int testIndex, final int gradeLevel) {
        final List<Float> listOfAvgGrades = new ArrayList<>();
        for (final Grade grade : getGradesOfLevel(gradeLevel)) {
            final Float avgFromGrade = grade.getSpecificMinMarksFromSubject(subject, testIndex);
            if (avgFromGrade != null) {
                listOfAvgGrades.add(avgFromGrade);
            }
        }
        Float avg = (float) listOfAvgGrades.stream().mapToDouble(x -> x).average().orElse(-1.0);
        if (avg == -1) {
            avg = null;
        }
        return avg;
    }

    /**
     * Returns the median mark from a specified test
     * in a specified subject on a specified level
     *
     * @param subject    {@link Subject}
     * @param testIndex  {@link Integer}
     * @param gradeLevel {@link Integer}
     * @return {@link Float} the median mark
     */
    public Float getSpecificMedianFromSubjectAndLevel(final Subject subject, final int testIndex, final int gradeLevel) {

        final List<Float> listOfMedianGrades = new ArrayList<>();
        Float median = null;

        for (final Grade grade : getGradesOfLevel(gradeLevel)) {

            final Float medianFromGrade = grade.getSpecificMedianMarksFromSubject(subject, testIndex);

            if (medianFromGrade != null) {

                listOfMedianGrades.add(medianFromGrade);

            }
        }

        if (!listOfMedianGrades.isEmpty()) {

            Collections.sort(listOfMedianGrades);
            int medianIndex = listOfMedianGrades.size() / 2;

            if (medianIndex % 2 == 1) {
                medianIndex = (int) Math.ceil(medianIndex);
            }

            median = listOfMedianGrades.get(medianIndex);

        }
        return median;
    }

    /**
     * returns the {@link Grade} with the corresponding level and identifier
     * if no {@link Grade} matches the request, a new one will be created
     * @param level {@link Integer}
     * @param identifier {@link Character}
     * @return grade {@link Grade}
     */
    public Grade getGradeFromLvlAndId(final int level, final char identifier) {
        final Optional<Grade> gradeEntry = grades.stream().filter(n -> n.getLevel() == level && n.getIdentifier() == identifier).findFirst();
        Grade grade;
        grade = gradeEntry.orElseGet(() -> new Grade(level, identifier));
        grades.add(grade);
        return grade;
    }

    /**
     * Parses a ".txt".
     * If the class does not exist: nothing will be added,
     * If the subject isn't written as in {@link Subject}:
     * the parser will return an error,
     * If the {@link Student} doesn't exist: a new one will be created
     * with the data specified in the parsed file.
     *
     * @param filename {@link String}
     */
    public void parseTxt(final String filename) {
        try {

            final File fileToParse = new File(filename);
            String line;
            int lineRead = 0;
            Grade grade = null;
            Subject subject = null;
            String[] list;
            int level = 0;
            char identifier;

            if (fileToParse.exists()) {
                final BufferedReader reader = new BufferedReader(new FileReader(fileToParse));
                while ((line = reader.readLine()) != null) {
                    lineRead++;
                    switch (lineRead) {
                        case 1:
                            level = line.charAt(0) - '0';
                            identifier = line.charAt(1);
                            grade = getGradeFromLvlAndId(level, identifier);
                            break;
                        case 2:
                            subject = Subject.valueOf(line);
                            break;
                        default:
                            list = line.split(" ");
                            grade.getStudentFromNameFirstnameLevel(list[1], list[0], level).addMark(subject, Float.parseFloat(list[2]));
                            break;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            System.out.println("la matière dans le fichier n'est pas référencée");

        }
    }
}
