package school;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Application {
    public static void main(final String[] args) {
        final MiddleSchool middleSchool = new MiddleSchool();
        middleSchool.parseTxt("testParser.txt");
        System.out.println(middleSchool.getGradeFromLvlAndId(6,'B'));

    }
}
