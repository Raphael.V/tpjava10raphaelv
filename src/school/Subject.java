package school;

public enum Subject {

    MATHEMATIQUES(false, 3, 6),
    FRANCAIS(false, 3, 6),
    ANGLAIS(false, 3, 6),
    HISTOIRE_GEO(false, 3, 6),
    PHYSIQUE(false, 3, 5),
    SCIENCE_NAT(false, 3, 6),
    ART(false, 3, 6),
    MUSIQUE(false, 2, 6),
    SPORT(false, 2, 6),
    LANGUE_VIVANTE(false, 3, 5),
    LATIN(true, 3, 6),
    GREC(true, 3, 6),
    ANGLAIS_AVANCE(true, 3, 6);

    private final boolean optional;
    private final int nbMarks;
    private final int level;

    Subject(final boolean optional, final int nbMarks, final int level) {
        this.optional = optional;
        this.nbMarks = nbMarks;
        this.level = level;
    }

    public boolean isOptional() {
        return optional;
    }

    public int getNbMarks() {
        return nbMarks;
    }

    public int getLevel() {
        return level;
    }
}
