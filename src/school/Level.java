package school;

public enum Level {
    SIXTHGRADE(6),
    SEVENTHGRADE(5),
    EIGHTHGRADE(4),
    NINTHGRADE(3);
    private final int level;

    Level(final int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
