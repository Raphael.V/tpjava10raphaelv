package school;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lombok.experimental.UtilityClass;

@UtilityClass
public class FileUtils {
    public static void generateFile(final String filename, final String values) {
        final File file = new File(filename);
        final File dir = file.getParentFile();
        if (!dir.exists()) {
            dir.mkdir();
        }
        try {
            if (file.exists()) {
                file.createNewFile();
            }
            final FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(values);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
