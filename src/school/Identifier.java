package school;

public enum Identifier {

    A('A'),
    B('B'),
    C('C'),
    D('D'),
    E('E'),
    F('F');

    private final char identifier;

    Identifier(final char identifier) {
        this.identifier = identifier;
    }

    public char getIdentifier() {
        return identifier;
    }
}
