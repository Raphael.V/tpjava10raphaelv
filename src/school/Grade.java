package school;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class Grade {

    private final List<Student> students;
    private final int level;
    private final char identifier;

    /**
     * Creates a new {@link Grade} from a level and id
     * and adds 20 {@link Student} to it
     * @param level {@link Integer}
     * @param identifier {@link Character}
     */
    public Grade(final int level, final char identifier) {
        this.level = level;
        this.identifier = identifier;
        this.students = new ArrayList<>();

        //adding students to grade

        for (int i = 0; i < 20; i++) {
            final Student student = new Student(level);
            this.students.add(student);
        }
    }

    /**
     * returns the list of students in the {@link Grade}
     *
     * @return students {@link List}({@link Student})
     */
    public List<Student> getStudents() {
        return students;
    }

    /**
     * returns the level of the {@link Grade}
     *
     * @return level {@link Integer}
     */
    public int getLevel() {
        return level;
    }

    /**
     * returns the identifier of the {@link Grade}
     *
     * @return identifier {@link Character}
     */
    public char getIdentifier() {
        return identifier;
    }

    @Override
    public String toString() {

        return "Grade " + level + identifier + " {" +
                " students=" + students +
                "}\n";
    }

    /**
     * Returns the global average mark for an entire {@link Grade}
     *
     * @return {@link Float}
     */
    public float getGlobalAvgForGrade() {
        float totalAvg = 0;
        final int count = students.size();
        for (final Student student : students) {
            totalAvg += student.getAvgGlobalMark();
        }
        return (float) Math.floor((totalAvg / count) * 10) / 10;
    }

    /**
     * returns the list of a specified test of the specified subject
     * @param subject {@link Subject}
     * @param index {@link Integer}
     * @return {@link List}({@link Float})
     */
    public List<Float> getSpecificMarksFromSubject(final Subject subject, final int index) {
        final List<Float> marksList = new ArrayList<>();
        Float mark;
        for (final Student student : students) {
            mark = student.getSpecificMarksFromSubject(subject, index);
            if (mark != null) {
                marksList.add(mark);
            }
        }
        return marksList;
    }

    /**
     * returns the minimum of a specified test of the specified subject
     *
     * @param subject {@link Subject}
     * @param index {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificMinMarksFromSubject(final Subject subject, final int index) {
        final List<Float> marksList = getSpecificMarksFromSubject(subject, index);
        Float minMark;
        if (marksList.isEmpty()) {
            minMark = null;
        } else {;
            minMark = Collections.min(marksList);
        }
        return minMark;
    }

    /**
     * returns the maximum of a specified test of the specified subject
     *
     * @param subject {@link Subject}
     * @param index {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificMaxMarksFromSubject(final Subject subject, final int index) {
        final List<Float> marksList = getSpecificMarksFromSubject(subject, index);
        Float maxMark;
        if (marksList.isEmpty()) {
            maxMark = null;
        } else {
            maxMark = Collections.max(marksList);
        }
        return maxMark;
    }

    /**
     * returns the average mark of a specified test of the specified subject
     *
     * @param subject {@link Subject}
     * @param index {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificAvgMarksFromSubject(final Subject subject, final int index) {
        final List<Float> marksList = getSpecificMarksFromSubject(subject, index);
        Float avgMark = (float) 0;
//        marksList.stream().mapToDouble(x->x).average().orElse(-1.0);
        if (marksList.isEmpty()) {
            avgMark = null;
        } else {
            avgMark = marksList.stream().reduce(avgMark, Float::sum);
            avgMark /= marksList.size();
        }
        return avgMark;
    }

    /**
     * returns the median of a specified test of the specified subject
     *
     * @param subject {@link Subject}
     * @param index {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificMedianMarksFromSubject(final Subject subject, final int index) {
        final List<Float> marksList = getSpecificMarksFromSubject(subject, index);
        Float medianMark;
        if (marksList.isEmpty()) {
            medianMark = null;
        } else {
            Collections.sort(marksList);
            int medianIndex = marksList.size() / 2;
            if (medianIndex % 2 == 1) {
                medianIndex = (int) Math.ceil(medianIndex);
            }
            medianMark = marksList.get(medianIndex);
        }
        return medianMark;
    }

    /**
     * returns the student corresponding to the parameters,
     * if no student is found, a new one will be created
     * @param firstname {@link String}
     * @param lastName {@link String}
     * @param level {@link Integer}
     * @return {@link Student}
     */
    public Student getStudentFromNameFirstnameLevel(final String firstname, final String lastName, final int level) {
        final Optional<Student> studentEntry = students.stream().filter(n -> n.getFirstname().equals(firstname) && n.getLastname().equals(lastName)).findFirst();
        Student student;
        student = studentEntry.orElseGet(() -> new Student(lastName, firstname, level));
        students.add(student);
        return student;
    }


}
