package school;

import com.github.javafaker.Faker;

import java.util.*;

public class Student {

    private final int level;
    private final String lastname;
    private final String firstname;
    private final Map<Subject, List<Float>> marks;

    /**
     * creates a {@link Student} from its grade
     *
     * @param level the grade of the {@link Student}
     */

    public Student(final int level) {

        final Faker faker = new Faker();

        this.level = level;
        this.lastname = faker.name().lastName();
        this.firstname = faker.name().firstName();
        this.marks = new HashMap<>();

        generateMarks();

    }

    /**
     * Constructor used during the import of a mark file
     * @param lastname {@link String}
     * @param firstname {@link String}
     * @param level {@link Integer}
     */
    public Student(final String lastname, final String firstname, final int level) {

        this.lastname = lastname;
        this.firstname = firstname;
        this.level = level;
        this.marks = new HashMap<>();

    }

    /**
     * returns the last name of the {@link Student}
     * @return {@link String}
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * returns the first name of the {@link Student}
     * @return {@link String}
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * returns the grade of the {@link Student}
     * @return {@link Integer}
     */
    public int getGrade() {
        return level;
    }

    /**
     * returns the map of marks of the {@link Student}
     * @return {@link Map} ({@link Subject},{@link List}({@link Float}) )
     */
    public Map<Subject, List<Float>> getMarks() {
        return marks;
    }

    @Override
    public String toString() {
        return "\nStudent{" +
                "\nsurname='" + lastname + "'" +
                ",\nfirstname='" + firstname + "'" +
                ",\nlevel=" + level +
                ",\nmarks=" + marks +
                "}";
    }

    /**
     * generates and adds all marks to the marks map
     */
    private void generateMarks() {
        final Random rand = new Random();
        for (final Subject subject : Subject.values()) {
            if (!subject.isOptional()) {
                generateMarkForSubject(subject);
            }
        }
        switch (rand.nextInt(6)) {
            case 1:
                generateMarkForSubject(Subject.ANGLAIS_AVANCE);
                break;
            case 2:
                generateMarkForSubject(Subject.LATIN);
                break;
            case 3:
                generateMarkForSubject(Subject.GREC);
                break;
            case 4:
                generateMarkForSubject(Subject.ANGLAIS_AVANCE);
                generateMarkForSubject(Subject.LATIN);
                break;
            case 5:
                generateMarkForSubject(Subject.ANGLAIS_AVANCE);
                generateMarkForSubject(Subject.GREC);
                break;
            default:
                break;
        }
    }

    /**
     * adds the randomly generated marks to the {@link Student}
     * using properties from the {@link Subject} enum
     *
     * @param subject {@link Subject}
     */
    private void generateMarkForSubject(final Subject subject) {
        if (level <= subject.getLevel()) {
            final Random rand = new Random();
            final List<Float> markList = new ArrayList<>();
            for (int i = 0; i < subject.getNbMarks(); i++) {
                //adds a random mark with one decimal
                markList.add((float) Math.floor((Math.abs(rand.nextGaussian() * 4 + 12) % 20) * 10) / 10);
            }
            marks.put(subject, markList);
        }
    }

    /**
     * adds a mark to an existing {@link Student},
     * if the {@link Subject} provided isn't in the marks
     * of the student and available for him to have:
     * adds the subject too
     *
     * @param subject {@link Subject}
     * @param mark {@link Float}
     * @return {@link Boolean} true if the mark was added, false otherwise
     */
    public boolean addMark(final Subject subject, final float mark) {
        boolean added = false;
        if (level <= subject.getLevel()) {
            if (marks.containsKey(subject)) {
                marks.get(subject).add(mark);
            } else {
                final List<Float> list = new ArrayList<>();
                list.add(mark);
                marks.put(subject, list);
            }
            added = true;
        }
        return added;
    }

    /**
     * returns a list containing the marks
     * of the {@link Student} in the specified subject
     * or null if no subjects match the parameter
     *
     * @param subject {@link Subject}
     * @return MarksList {@link List<Float>}
     */
    private List<Float> getMarksFromSubject(final Subject subject) {
        return marks.get(subject);
    }

    /**
     * returns the mark of the {@link Student}
     * of the provided test in the provided subject
     * or null if no subjects match the parameter
     * or if the index provided exceed number of test
     *
     * @param subject {@link Subject}
     * @param index   {@link Integer}
     * @return {@link Float}
     */
    public Float getSpecificMarksFromSubject(final Subject subject, final int index) {
        Float mark = null;
        if (!(getMarksFromSubject(subject) == null || index > subject.getNbMarks())) {
            mark = getMarksFromSubject(subject).get(index);
        }
        return mark;
    }

    /**
     * Returns the global average mark
     * of the {@link Student}
     * taking in account optional subject notation,
     * 1 point above 10 on the average mark
     * of the optional subject adds 0.1 point
     * on the global average mark meaning up to
     * 2 points can be added
     *
     * @return globalAverage {@link Float}
     */
    public float getAvgGlobalMark() {
        float avg = 0;
        float pointsToAdd = 0;
        int count = 0;
        for (final Map.Entry<Subject, List<Float>> subjectListEntry : marks.entrySet()) {
            if (subjectListEntry.getKey().isOptional()) {

                float optionSum = 0;
                final int optionCount = subjectListEntry.getValue().size();
                optionSum = subjectListEntry.getValue().stream().reduce(optionSum, Float::sum);

                final float optionAvg = Math.round(optionSum / optionCount - 10);
                if (optionAvg >= 0) {
                    pointsToAdd += optionAvg / 10;
                }

            } else {
                avg = subjectListEntry.getValue().stream().reduce(avg, Float::sum);
                count += subjectListEntry.getValue().size();
            }
        }
        return (float) Math.floor((avg / count + pointsToAdd) * 10) / 10;
    }
}
